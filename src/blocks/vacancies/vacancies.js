var vacancies = new ScrollMagic.Scene({
  triggerElement: '.vacancies__list',
  triggerHook: "0",
  duration: '300%',
  reverse: true
})

$(window).on('load resize', function () {

  if ($(window).width() >= 1200) {
    vacancies.setPin('.vacancies__list')
    vacancies.addTo(controller)

    var vacanciesTimeline = new TimelineMax();

    var vacancies01 = TweenMax.to(".vacancies__cursor", 1, {
      x: '5vw',
      ease: 'power4.inOut',
      y: '30vh'
    });

    var vacancies02 = TweenMax.to(".vacancies__cursor", 1, {
      x: '70vw',
      ease: 'power4.inOut',
      y: '25vh',
      delay: 1
    });

    var vacancies03 = TweenMax.to(".vacancies__cursor", 1, {
      x: '30vw',
      ease: 'power4.inOut',
      y: '70vh',
      delay: 1
    });

    var vacancies04 = TweenMax.to(".vacancies__cursor", 1, {
      x: '25vw',
      ease: 'power4.inOut',
      y: '65vh',
      delay: 1
    });

    var vacancies05 = TweenMax.to(".vacancies__cursor", .1, {
      scale: '.9',
      ease: 'power4.inOut',
      opacity: .143
    });

    var vacancies06 = TweenMax.to(".vacancies__cursor", .1, {
      x: '40vw',
      ease: 'power4.inOut',
      y: '30vw',
    });

    var vacancies1 = TweenMax.to(".vacancies__list-content", 1, {
      opacity: '0',
      delay: 1
    });

    var vacancies2 = TweenMax.to(".vacancies__content", 1, {
      scale: '1',
      x: '0',
      y: '0',
      opacity: '1',
      delay: -1
    });

    var vacancies3 = TweenMax.to(".vacancies__dscr", .5, {
      opacity: '1',
    });

    vacanciesTimeline.add(vacancies01).add(vacancies02).add(vacancies03).add(vacancies04).add(vacancies05).add(vacancies1).add(vacancies2).add(vacancies3).add(vacancies06);

    vacancies.setTween(vacanciesTimeline);

  } else {
    vacancies.destroy(true);
  }
})
