$('.data__tabs-item').click(function(e) {
  e.preventDefault();
  tabChange($(this).index());
  $('.data__tabs-item').removeClass('data__tabs-item--clicked');
  $(this).addClass('data__tabs-item--clicked');
});

var tabChange = function(number) {
  var tabLink = $('.data__tabs-item').eq(number);
  $('.data__tabs-item').removeClass('data__tabs-item--active');
  tabLink.addClass('data__tabs-item--active');
  $('.data__tabs-content').removeClass('data__tabs-content--active');
  $('.data__tabs-content').eq(number).addClass('data__tabs-content--active');
}


$('.data').each(function (i, el) {
  var tabLink1 = $('.data__tabs-item').eq(0);
  var tabLink2 = $('.data__tabs-item').eq(1);
  var dataTabs = new ScrollMagic.Scene({
      triggerElement: el,
      triggerHook: 0,
      reverse: true,
      duration: '200%'
    })
    .on('progress', function (event) {
      
      if(event.progress < 0.5) {
        
        if (!tabLink2.hasClass('data__tabs-item--clicked')) {
          
          if(event.scrollDirection == 'REVERSE') {
            tabChange(0);
          }
        }
      }
      
      else if(event.progress >= 0.5) {
        
        if (!tabLink2.hasClass('data__tabs-item--clicked')) {
          
          if(event.scrollDirection == 'FORWARD') {
            tabChange(1);
          }
        }
      }
    })
    .setPin(el)
    .addTo(controller)
});
