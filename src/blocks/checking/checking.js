$('.checking__content').each(function (i, el) {
  var checkingReveal = new ScrollMagic.Scene({
      triggerElement: el,
      triggerHook: ".8",
      reverse: true,
      duration: '40%'
    })
    .setTween(el, {
      y: '0',
      opacity: '1'
    })
    .addTo(controller)

  function callback(event) {
//    console.log("Event fired! (" + event.type + ")");
    if (event.type == 'end') {
      $('.checking').addClass('checking--animated');
    }
    
    else if (event.type == 'start') {
      $('.checking').removeClass('checking--animated');
    }
  }
  checkingReveal.on("change update progress start end enter leave", callback);
});
