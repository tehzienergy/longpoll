const slider3 = new ScrollBooster({
  viewport: document.querySelector('.feedback__content'),
  content: document.querySelector('.feedback__wrapper'),
  scrollMode: 'transform',
  direction: 'horizontal',
});
