$('.js-form-btn').click(function(e) {
  e.preventDefault();
  $('.modal').addClass('modal--active');
});

$('.modal__close').click(function(e) {
  e.preventDefault();
  $('.modal').removeClass('modal--active');
});

$('.js-modal-back').click(function(e) {
  e.preventDefault();
  $('.modal').removeClass('modal--active');
});
