$('.header__language').click(function(e) {
  e.preventDefault();
  var languageItem = $(this).find('span.en');
  if (languageItem.hasClass('active')) {
    $('.wrapper').addClass('wrapper--en');
    $('.header__language span').toggleClass('active');
    translateAnswers('en');
  }
  else {
    $('.wrapper').removeClass('wrapper--en');
    $('.header__language span').toggleClass('active');
    translateAnswers('ru');
  }
})

var userLang = navigator.language || navigator.userLanguage; 


var languageSwitch = function(language) {
  if (language == 'ru-RU') {
    $('.wrapper').removeClass('wrapper--en');
    $('.header__language span.ru').removeClass('active');
    $('.header__language span.en').addClass('active');
    translateAnswers('ru');
  }
  else {
    $('.wrapper').addClass('wrapper--en');
    $('.header__language span.en').removeClass('active');
    $('.header__language span.ru').addClass('active');
    translateAnswers('en');
  }
}

languageSwitch(userLang);
