$(function() {
  $('.stats__item-number').each(function() {
    $(this).appear();
  })
  $(document.body).on('appear', $('.stats__item-number'), function(event, el) {
    var count = el.data('count');
    var countID = el.attr('id');
    var number = el.data('number');
    var count = function(countName, id, finalNumber) {
      var options = {
        separator: " ",
        useEasing: true,
      };
      var countItem = new CountUp(id, finalNumber, options);
      countItem.start();
    }
    count('countUp' + count, countID, number);
  });
});
