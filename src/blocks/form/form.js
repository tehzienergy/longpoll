const db = firebase.firestore();

$('.form__btn').click(function (e) {
  e.preventDefault();
  var username = $('input[name="email"]').val();
  var phone = $('input[name="phone"]').val();
  
  db.collection('longpoll').add({
    name: username,
    phone: phone
  });
  
  $('.modal__container').hide();
  
  $('.modal__success').fadeIn('fast');
})
