const slider1 = new ScrollBooster({
  viewport: document.querySelector('.steps__slider-wrapper--first'),
  content: document.querySelector('.js-slider-candidates .slider__wrapper'),
  scrollMode: 'transform',
  direction: 'horizontal',
});

const slider2 = new ScrollBooster({
  viewport: document.querySelector('.steps__slider-wrapper--second'),
  content: document.querySelector('.js-slider-teams .slider__wrapper'),
  scrollMode: 'transform',
  direction: 'horizontal',
});
