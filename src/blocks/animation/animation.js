$(window).on('resize', function() {
  var windowWidth = $(window).width();
  
  if (windowWidth >= 1200) {
    location.reload();
  }
});

var controller = new ScrollMagic.Controller();

$('.animation--reveal').each(function (i, el) {
  var magicReveal = new ScrollMagic.Scene({
      triggerElement: el,
      triggerHook: ".8",
      reverse: true,
      duration: '40%'
    })
    .setTween(el, {
      y: '0',
      opacity: '1'
    })
    .addTo(controller)
});

$('.animation--reveal-right').each(function (i, el) {
  var magicReveal = new ScrollMagic.Scene({
      triggerElement: el,
      triggerHook: ".8",
      reverse: true,
      duration: '40%'
    })
    .setTween(el, {
      x: '0',
      opacity: '1'
    })
    .addTo(controller)
});

$('.animation--reveal-delayed').each(function (i, el) {
  var magicReveal = new ScrollMagic.Scene({
      triggerElement: el,
      triggerHook: ".6",
      reverse: true,
      duration: '20%'
    })
    .setTween(el, {
      y: '0',
      opacity: '1'
    })
    .addTo(controller)
});

$('.animation--reveal-progressive').each(function (i, el) {
  var revealDelay = 1 - i / 10;
  var magicReveal = new ScrollMagic.Scene({
      triggerElement: el,
      triggerHook: revealDelay,
      reverse: true,
      duration: '40%'
    })
    .setTween(el, {
      y: '0',
      opacity: '1'
    })
    .addTo(controller)
});

var horizontalScroll = function (id, el, screens) {
  var tl = new TimelineMax();

  var elementWidth = document.getElementById(id).offsetWidth * screens;

  var width = window.innerWidth - elementWidth;

  var duration = elementWidth / window.innerHeight * 100;

  var official = duration + '%';

  tl
    .to(el, 5, {
      x: width,
      ease: 'power4.out',
//      delay: 2
    });

  var horizontalScrolling = new ScrollMagic.Scene({
      triggerElement: el,
      triggerHook: 0,
      duration: official
    })
    .setPin(el)
    .setTween(tl)
    .addTo(controller);

  $(window).on('load resize', function () {

    if ($(window).width() < 768) {
      horizontalScrolling.destroy('true');
    } 
  });
}


$(window).on('load resize', function () {

  if ($(window).width() >= 768) {
    horizontalScroll('stats-container', '.stats__container', 3);
  } 
  else {
    vacancies.destroy(true);
  }
});

$(window).on('load resize', function () {

  if ($(window).width() >= 768) {
    horizontalScroll('teamwork-container', '.teamwork__container', 2);
  } 
  else {
    vacancies.destroy(true);
  }
})


$(window).on('load resize', function () {

  if ($(window).width() >= 1200) {
    $('.steps__item-bg').each(function (i, el) {
      var stepsBG = new ScrollMagic.Scene({
          triggerElement: el,
          triggerHook: 'onEnter',
          reverse: true,
          duration: '50%'
        })
        .setTween(el, {
          scale: '1',
          ease: Power0.easeNone
        })
        .addTo(controller)
    });
  }
});



var answerReveal = new ScrollMagic.Scene({
    triggerElement: '.steps__question-answer--1',
    triggerHook: ".6",
    reverse: false
  })
  .setTween('.steps__question-answer--1', {
    y: '0',
    opacity: '1'
  })
  .on('start', function () {
    var typeIt = new TypeIt(".steps__question-answer--1", {
      strings: answers[0],
      speed: 50,
      afterComplete: instance => {
        typeIt.destroy();
        var typeIt2 = new TypeIt(".steps__question-answer--2", {
          strings: answers[1],
          speed: 50,
          afterComplete: instance => {
            typeIt2.destroy();
            var typeIt3 = new TypeIt(".steps__question-answer--3", {
              strings: answers[2],
              speed: 50,
            }).go();
          }
        }).go();
      },
    }).go();
  })
  .addTo(controller);

var answerBig = {
  ru: "Вопрос 1. Какие технологические решения в нее точно войдут? Основная идея - комната будущего гибко трансформируется под потребности человека. Все пространство устроено так, что оно заботится обо всех органах чувств человека, тонко подстраиваясь под его психологическое и физическое состояние. При входе в комнату получаем информацию следующего содержания: «Будущее рождается сегодня!» - как аудио или ненавязчиво отображенный приятно выплывающий текст где-то на стене или потолке. Эта идея ненавязчиво повторяется в указанном пространстве несколько раз. Используются следующие технологические решения: 1. Искусственный интеллект (AI) для обеспечения оптимального микроклимата в комнате и поддерживающей человека информационной среды...",
  en: "Question 1. What technological solutions will definitely be a part of it? I approach this concept of “room of the future” from the perspective of Maslow's pyramid of needs. The pyramid reflects one of the most popular and well-known theories of motivation (the hierarchy of needs theory) and it has seven levels. I have matched technological solutions to each level. As a room, I chose a room for living and working - that is, not a kitchen or a bathroom. <br>Level 1. Physical needs. For this level, measurement and control of the parameters of the internal climate — humidity, temperature, and illumination — will be relevant. These parameters will change dynamically depending on the time of year, time of day, and number of people in the room. A very important element is a comfortable bed with sleep monitoring features, therapeutic and preventive massage. The composition of air is no less important, so there should be air conditioners, filters, green plants…"
}


var answer1 = {
  ru: "Miro, Slack, TechCrunch",
  en: 'Telegram, Vedomosti, TechCrunch'
}

var answer2 = {
  ru: "Олег Якубенков — симулятор Gopractice. Роман Абрамов — директор по продукту в CarPrice. Павел Дуров — Telegram, VK",
  en: 'Evgeny Kuznetsov, Andrey Sebrant, Andrey Raigorodsky'
}

var answer3 = {
  ru: "Реорганизовал работу компании",
  en: 'Building the unit’s work - forming a funnel of projects, optimizing the staff, developing an action plan aimed at developing internal and external innovations.'
}

var answers = [answer1.ru, answer2.ru, answer3.ru, answerBig.ru];

var translateAnswers = function(lang) {
  if (lang == 'en') {
    answers = [answer1.en, answer2.en, answer3.en, answerBig.en];
  }
  else if (lang == 'ru') {
    answers = [answer1.ru, answer2.ru, answer3.ru, answerBig.ru];
  }
}

var answerBigReveal = new ScrollMagic.Scene({
    triggerElement: '.steps__question-answer--big',
    triggerHook: ".6",
    reverse: false
  })
  .setTween('.steps__question-answer--big', {
    y: '0',
    opacity: '1'
  })
  .on('start', function () {
    var typeItBig = new TypeIt(".steps__question-answer--big", {
      strings: answers[3],
      speed: 50,
    }).go();
  })
  .addTo(controller);



